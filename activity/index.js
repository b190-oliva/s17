/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function userInfo(){
		let userName = prompt("What is your name?");
		let userAge = prompt("How old are you?");
		let userAddress = prompt("Where do you live?");
		alert("Thank you for the information!");
		console.log("Hello, "+userName);
		console.log("You are "+userAge+" years old.");
		console.log("You live at "+userAddress);
	};
	userInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function bandLists(){
		let listsOfBands0 = "1. Metallica";
		console.log(listsOfBands0);
		let listsOfBands1 = "2. Greenday";
		console.log(listsOfBands1);
		let listsOfBands2 = "3. My Chemical Romance";
		console.log(listsOfBands2);
		let listsOfBands3 = "4. Yellowcard";
		console.log(listsOfBands3);
		let listsOfBands4 = "5. Red Jumpsuit Apparatus";
		console.log(listsOfBands4);
	};
	bandLists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function movieLists(){
		let movie0 = "1. City of Ember (2008)";
		console.log(movie0+"\nRotten tomatoes rating :54%");
		let movie1 = "2. Accepted (2021)";
		console.log(movie1+"\nRotten tomatoes rating :100%");
		let movie2 = "3. Inception(2010)";
		console.log(movie2+"\nRotten tomatoes rating :91%");
		let movie3 = "4. Catch me if you can (2002)";
		console.log(movie3+"\nRotten tomatoes rating :89%");
		let movie4 = "5. The green mile (1999)"
		console.log(movie4+"\nRotten tomatoes rating :94%");
	}
	movieLists();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/
	
	
	let printFriends = function(){
		alert("Hi! Please add the names of your friends.");
		let friend1 = prompt("Enter your first friend's name:"); 
		let friend2 = prompt("Enter your second friend's name:"); 
		let friend3 = prompt("Enter your third friend's name:");

		console.log("You are friends with:")
		console.log(friend1); 
		console.log(friend2); 
		console.log(friend3); 
	};
	printFriends();

	// console.log(friend1);
	// console.log(friend2);