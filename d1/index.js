//Javascript basic function

function printName(){
	let firstName = ["Tony", "John", "Chris", "Thor"];
	console.log(firstName[3]);
	firstName = "Thor";
	if(firstName[2]=="Chris"){
		console.log("Iron Man");
	}
	else{
		console.log(firstName);
	}
};
printName();

//hoisting
declaredFunction();

function declaredFunction(){
	console.log("Declaring Function");
}

//Function expression

let variablefunction = function(){
	console.log("Hello again Captain!");
}

variablefunction();

function function1(){
	console.log("Demon Slayer,", "Fate stay night,", "Re:Zero");
}
function1();
function function2(){
	console.log("Ember,", "Minions,", "Pets")
};
function2();

declaredFunction = function(){
	console.log("updated DeclaredFunction")
};
declaredFunction();

// We cannot change the declared functions/function expression that are defined using const

const constFunction= function(){
	console.log("Initialized const function");
};
constFunction();

// constFunction = function(){
// 	console.log("Cannot be re-assigned");
// };
// constFunction();


// Function scoping
// - scope is the accesibility of variables/functions

//private - curly braces = code block
{
	let localVar = "Efren Bata Reyes"
}
// local
let globalVar = "WWE";

console.log(globalVar);
// console.log(localVar); - will not work

//function scope

function showNames(){
	var functionVar = "Joe";
	const functionConst = "John Cena";
	let functionLet = "Jane"

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}
showNames();
// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

/*
	functionVar, functionConst, functionLet - are function-scoped and cannot be accessed outside the function they are declared
*/

// Nested Functions

function newFunction(){
	let name = "April";

	function nestedFunction(){
		let nestedName = "John Cena";
		console.log(nestedName);
	}
	console.log(name);
	nestedFunction();
};

newFunction();
// nestedFunction(); - returns an error

//public
let publicVoid = "Bengbong";
//private
let variable = function(){
	console.log(publicVoid);
	let privateVoid = "YohannBoy";
	function privateSub(){
		let private = "Yohannbadboy";
		console.log(private);
	};
	privateSub();
	console.log(privateVoid);
};
variable();

// Using alert()

// alert("Hello world!");

// function showSample(){
// 	alert("Hello again!");
// };
// //this can be called later
// // showSample();

// console.log("I will be displayed after the alert has been closed");

// using prompt()

// let samplePrompt = prompt("Enter your name.");

// console.log("Hello " +samplePrompt);

// let nullPrompt = prompt("Do not enter anything here");
// console.log(nullPrompt);

// function miniActivity(){
// 	let prompt1 = prompt("Please enter your first name:");
// 	let prompt2 = prompt("Please enter your last name:");
// 	console.log("Hello "+prompt1+" "+prompt2);
// }
// miniActivity();

// function naming

function getCourses(){
	let courses = ["Science","Math", "English"];
	console.log(courses);
};
getCourses();

function get(){
	let name = "Jamie";
	console.log(name);
};
get();

function foo(){
	console.log(25%5);
};
foo();